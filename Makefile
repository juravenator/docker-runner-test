SHELL:=/usr/bin/env bash -o pipefail
.DEFAULT_GOAL := help

include .make/help.mk

##
### main targets
##

gitlab-runner-linux-amd64: # fetch the runner
	curl -LO https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
	chmod +x gitlab-runner-linux-amd64

runner.register: gitlab-runner-linux-amd64 ## register the runner, run once
ifndef RUNNER_TOKEN
	$(error "No RUNNER_TOKEN variable set, cannot register runner")
endif
	./gitlab-runner-linux-amd64 register \
		--template-config config-template.toml \
		--config config-rendered.toml \
		--registration-token $${RUNNER_TOKEN} \
		--tag-list jura \
		--non-interactive

runner.start: ## start the runner
	./gitlab-runner-linux-amd64 run --config config-rendered.toml
