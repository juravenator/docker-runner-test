# docker-runner-test

config and test CI job for a docker runner that needs to be able to serve http content and login to other hosts consuming said content

## GitLab Runner

With the Makefile, you can register and run a local GitLab runner.
It will be configured in docker executor mode; and run with host networking.